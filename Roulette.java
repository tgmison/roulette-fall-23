import java.util.Random;
import java.util.Scanner;

public class Roulette {

    public static void main(String[] args) {
        // Create a RouletteWheel object.
        RouletteWheel rouletteWheel = new RouletteWheel();

        // Every user starts out with 1000$.
        int userMoney = 1000;

        // Create a Scanner object to read user input.
        Scanner scanner = new Scanner(System.in);

        // Ask the user whether they would like to make a bet.
        System.out.println("Would you like to make a bet? (y/n)");
        String bet = scanner.nextLine();

        // If the user enters "y", ask them what number they would like to bet on.
        if (bet.equals("y")) {
            System.out.println("What number would you like to bet on? (0-36)");
            int betNumber = scanner.nextInt();

            // Ensure that the bet is not more money than the user currently has.
            if (betNumber > userMoney) {
                System.out.println("You don't have enough money to make that bet.");
                return;
            }

            // Spin the roulette wheel.
            rouletteWheel.spin();

            // Check if the user won or lost.
            boolean didWin = rouletteWheel.checkWin(betNumber);

            // Determine how much the user won or lost.
            int winnings = 0;
            if (didWin) {
                if (betNumber == 0 || betNumber == 36) {
                    winnings = betNumber * 35;
                } else {
                    winnings = betNumber * 2;
                }
            } else {
                winnings = -betNumber;
            }

            // Update the user's money.
            userMoney += winnings;

            // Print the results of the bet.
            System.out.println("You bet $" + betNumber + " on number " + betNumber + ".");
            System.out.println("The winning number was " + rouletteWheel.getValue() + ".");
            if (didWin) {
                System.out.println("You won $" + winnings + ".");
            } else {
                    
                }
            }
        }
    }

class RouletteWheel {
    private Random random;
    private int number;

    public RouletteWheel() {
        random = new Random();
        number = 0;
    }

    public boolean checkWin(int betNumber) {
        return false;
    }

    public void spin() {
        number = random.nextInt(37);
    }

    public int getValue() {
        return number;
    }

}